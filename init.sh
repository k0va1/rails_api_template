# /bin/bash

grep -rlI 'rails_api_template' . --exclude init.sh --exclude README.md | xargs sed -i '' -e "s/rails_api_template/$PROJECT_NAME/g"
sed -i '' -e "s/RailsApiTemplate/$PROJECT_CAMELCASE_NAME/g" ./config/application.rb
sed -i '' -e "s/\$username/$USER" ./config/database.yml
bundle install
